# MvpdDemo

#### Description
# MvpDemo  
MVP架构在Android的使用。Okhttp3、Retrofit2、Rxjava2 ，AutoDispose解决RxJava内存泄漏  

简书地址：https://www.jianshu.com/p/ae0b21d3238a  

本Demo由项目中提取修改  

使用的第三方库:  
    //butterknife  
    implementation 'com.jakewharton:butterknife:8.8.1'  
    annotationProcessor 'com.jakewharton:butterknife-compiler:8.8.1'  
    //okhttp3  
    implementation 'com.squareup.okhttp3:okhttp:3.10.0'  
    implementation "com.squareup.okhttp3:logging-interceptor:3.10.0"  
    //retrofit2  
    implementation 'com.squareu

#### Software Architecture
Software architecture description

#### Installation

1.  xxxx
2.  xxxx
3.  xxxx

#### Instructions

1.  xxxx
2.  xxxx
3.  xxxx

#### Contribution

1.  Fork the repository
2.  Create Feat_xxx branch
3.  Commit your code
4.  Create Pull Request


#### Gitee Feature

1.  You can use Readme\_XXX.md to support different languages, such as Readme\_en.md, Readme\_zh.md
2.  Gitee blog [blog.gitee.com](https://blog.gitee.com)
3.  Explore open source project [https://gitee.com/explore](https://gitee.com/explore)
4.  The most valuable open source project [GVP](https://gitee.com/gvp)
5.  The manual of Gitee [https://gitee.com/help](https://gitee.com/help)
6.  The most popular members  [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
