# MvpdDemo

#### 介绍
# MvpDemo  
MVP架构在Android的使用。Okhttp3、Retrofit2、Rxjava2 ，AutoDispose解决RxJava内存泄漏  

简书地址：https://www.jianshu.com/p/ae0b21d3238a  

本Demo由项目中提取修改  

使用的第三方库:  
    //butterknife  
    implementation 'com.jakewharton:butterknife:8.8.1'  
    annotationProcessor 'com.jakewharton:butterknife-compiler:8.8.1'  
    //okhttp3  
    implementation 'com.squareup.okhttp3:okhttp:3.10.0'  
    implementation "com.squareup.okhttp3:logging-interceptor:3.10.0"  
    //retrofit2  
    implementation 'com.squareu

#### 软件架构
软件架构说明


#### 安装教程

1.  xxxx
2.  xxxx
3.  xxxx

#### 使用说明

1.  xxxx
2.  xxxx
3.  xxxx

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 码云特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5.  码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
